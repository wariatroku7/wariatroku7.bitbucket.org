var app = angular.module('adamCzarnocki',[])

app.directive('navbarTransparent', function(){
  return {
    restrict: 'E',
    templateUrl: '/templates/navbar_transparent.html'
  };
});
app.directive('landing', function(){
  return {
    restrict: 'E',
    templateUrl: '/templates/landing.html'
  };
});
app.directive('contactForm', function(){
  return {
    restrict: 'E',
    templateUrl: '/templates/contact/contact_form.html'
  };
});
app.directive('foot', function(){
  return {
    restrict: 'E',
    templateUrl: '/templates/footer.html'
  };
});

console.log("Dupa");
